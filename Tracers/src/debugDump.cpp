#include "tracerUtils.h"
#include "Dumper.h"

using namespace std;

class DebugDumper: public virtual Dumper {
	bool enabled;
	ofstream traceFile;
	controlFlowTrace trace;
	INSTLIB::ICOUNT instructionCount;

public:
	DebugDumper () {
		enabled = false;
	}

	void init (string outputFileNamePrefix) {
		traceFile.open((outputFileNamePrefix + ".debug").c_str());
	}

	void checkPointDump (string dumpSuffix, THREADID threadID, const CONTEXT* ctx) {
		// ignored for now
		// TODO: serialize the partial trace so far to an extra file?
	}

	void startTracing (THREADID threadID, const CONTEXT* ctx) {
		enabled = true;
		trace.add(0, getProgramCounterValue(ctx), false);
		instructionCount.Activate(INSTLIB::ICOUNT::ModeBoth);
		// TODO: see how to make PIN add the instrumentation immediately and not only after leaving the already compiled
		// trace starting here at the moment the instrumentation will not begin at the specified address
		INS_AddInstrumentFunction(instrumentInstructions, this);
	}

	void stopTracing (THREADID threadID, const CONTEXT* ctx) {
		trace.add(getProgramCounterValue(ctx), 0, false);
		// TODO: see how it is possible to remove the instrumentation
		enabled = false;
		traceFile << "Executed instructions: " << instructionCount.Count();
		traceFile.close();
	}

private:
	static void debugDumpInstruction (string* instruction, void* dumperObject) {
		DebugDumper* dumper = static_cast<DebugDumper*>(dumperObject);
		if (!dumper->enabled)
			return;
		dumper->traceFile << *instruction << endl;
	}

	static void recordControlFlow (ADDRINT ip, ADDRINT target, bool trueOrFalseBranch, void* dumperObject) {
		DebugDumper* dumper = static_cast<DebugDumper*>(dumperObject);
		if (!dumper->enabled)
			return;
		dumper->traceFile << boolalpha << trueOrFalseBranch << endl;
	}

	static void recordFalseBranch (ADDRINT ip, ADDRINT target, void* dumperObject) {
		recordControlFlow(ip, target, false, dumperObject);
	}

	static void recordTrueBranch (ADDRINT ip, ADDRINT target, void* dumperObject) {
		recordControlFlow(ip, target, true, dumperObject);
	}

	// Pin calls this function every time a new instruction is encountered
	static VOID instrumentInstructions (INS insn, void* dumperObject) {
		ADDRINT address = INS_Address(insn);
		string instruction = INS_Disassemble(insn);
		string instructionDump;
		instructionDump.append(toHex(address) + ": " + "  " + instruction);
		string* instructionString = new string(instructionDump);
		INS_InsertCall(insn, IPOINT_BEFORE, (AFUNPTR) debugDumpInstruction, IARG_PTR, instructionString, IARG_PTR,
				dumperObject, IARG_END);
		// only instrument instructions where the target is not known statically -- i.e. computed and conditional jumps
		if (INS_IsBranchOrCall(insn)) {
			if (INS_HasFallThrough(insn)) {
				INS_InsertCall(insn, IPOINT_AFTER, (AFUNPTR) recordFalseBranch, IARG_INST_PTR, IARG_FALLTHROUGH_ADDR,
						IARG_PTR, dumperObject, IARG_END);
			}
			INS_InsertCall(insn, IPOINT_TAKEN_BRANCH, (AFUNPTR) recordTrueBranch, IARG_INST_PTR,
					IARG_BRANCH_TARGET_ADDR, IARG_PTR, dumperObject, IARG_END);
		}
	}

};
