#ifndef DUMPER_H_
#define DUMPER_H_

#include <string>
#include "pin.H"

/**
 * An interface for tools that capture and dump various trace and program data.
 */
class Dumper {
public:
	/**
	 * Set the prefix of the filename that this tool will use to dump its data to.
	 */
	virtual void init (std::string outputFileNamePrefix) = 0;

	/**
	 * Start the trace recording. The tool can be activated on a certain event in the program. After activation the tool
	 * will be able to record or dump recorded data until it is disabled by the stopTracing call.
	 */
	virtual void startTracing (LEVEL_VM::THREADID threadID, const LEVEL_VM::CONTEXT* ctx) = 0;

	/**
	 * Stop the trace recording. The tool will be disabled for the rest of the execution of the program. If the tool needs
	 * to write any of the recorded data so far then it should do it here. The tool won't be activated anymore after it
	 * has been stopped.
	 */
	virtual void stopTracing (LEVEL_VM::THREADID threadID, const LEVEL_VM::CONTEXT* ctx) = 0;

	/**
	 * Dump the data that this tool recorded up to now or will record now to the output file with the given suffix.
	 * This function can be called at various checkpoints during the trace. The dumper tool can decide what of the
	 * aggregated data it will dump and if it acts to this at all or only on the final stopTracing call.
	 */
	virtual void checkPointDump (std::string dumpSuffix, LEVEL_VM::THREADID threadID, const LEVEL_VM::CONTEXT* ctx) = 0;

	/**
	 * Destroy and remove any data structure of the tool.
	 */
	virtual ~Dumper () {
	}
};

#endif /* DUMPER_H_ */
