#include "tracerUtils.h"
#include "Dumper.h"

using namespace std;

class JmpsDumper: public virtual Dumper {
	bool enabled;
	ofstream dumpFile;
	controlFlowTrace trace;
	INSTLIB::ICOUNT instructionCount;

public:
	JmpsDumper () {
		enabled = false;
	}

	void init (string outputFileNamePrefix) {
		dumpFile.open((outputFileNamePrefix + ".flow").c_str());
	}

	void checkPointDump (string dumpSuffix, THREADID threadID, const CONTEXT* ctx) {
		// ignored for now
		// TODO: serialize the partial trace so far to an extra file?
	}

	void startTracing (THREADID threadID, const CONTEXT* ctx) {
		enabled = true;
		trace.add(0, getProgramCounterValue(ctx), false);
		// TODO: see how to make PIN add the instrumentation immediately and not only after leaving the already compiled
		// trace starting here at the moment the instrumentation will not begin at the specified address
		INS_AddInstrumentFunction(instrumentInstructions, this);
	}

	void stopTracing (THREADID threadID, const CONTEXT* ctx) {
		trace.add(getProgramCounterValue(ctx), 0, false);
		// TODO: see how it is possible to remove the instrumentation instead
		enabled = false;
		trace.serialize(dumpFile);
		dumpFile.close();
	}

private:
	static void recordControlFlow (ADDRINT ip, ADDRINT target, bool trueOrFalseBranch, void* dumperObject) {
		JmpsDumper* dumper = static_cast<JmpsDumper*>(dumperObject);
		if (!dumper->enabled)
			return;
		dumper->trace.add(ip, target, trueOrFalseBranch);
	}

	static void recordFalseBranch (ADDRINT ip, ADDRINT target, void* dumperObject) {
		recordControlFlow(ip, target, false, dumperObject);
	}

	static void recordTrueBranch (ADDRINT ip, ADDRINT target, void* dumperObject) {
		recordControlFlow(ip, target, true, dumperObject);
	}

	// Pin calls this function every time a new instruction is encountered
	static VOID instrumentInstructions (INS insn, void* dumperObject) {
		instrumentAllControlTransfers(insn, dumperObject);
//		instrumentOnlyUnknownControlTransfers(insn, dumperObject);
	}

	// only instrument instructions where the target is not known statically -- i.e. computed and conditional jumps
	static VOID instrumentOnlyUnknownControlTransfers (INS insn, void* dumperObject) {
		if (INS_IsIndirectBranchOrCall(insn) || (INS_IsDirectBranchOrCall(insn) && INS_HasFallThrough(insn))) {
			if (INS_HasFallThrough(insn)) {
				INS_InsertCall(insn, IPOINT_AFTER, (AFUNPTR) recordFalseBranch, IARG_INST_PTR, IARG_FALLTHROUGH_ADDR,
						IARG_PTR, dumperObject, IARG_END);
			}
			INS_InsertCall(insn, IPOINT_TAKEN_BRANCH, (AFUNPTR) recordTrueBranch, IARG_INST_PTR,
					IARG_BRANCH_TARGET_ADDR, IARG_PTR, dumperObject, IARG_END);
		}
	}

	// instrument all jumps and calls (even when the jumps are not conditional and the address is hard coded)
	static VOID instrumentAllControlTransfers (INS insn, void* dumperObject) {
		if (INS_IsBranchOrCall(insn)) {
			if (INS_HasFallThrough(insn)) {
				INS_InsertCall(insn, IPOINT_AFTER, (AFUNPTR) recordFalseBranch, IARG_INST_PTR, IARG_FALLTHROUGH_ADDR,
						IARG_PTR, dumperObject, IARG_END);
			}
			INS_InsertCall(insn, IPOINT_TAKEN_BRANCH, (AFUNPTR) recordTrueBranch, IARG_INST_PTR,
					IARG_BRANCH_TARGET_ADDR, IARG_PTR, dumperObject, IARG_END);
		}
	}

};
