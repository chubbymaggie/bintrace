#ifndef INC_TRACER_UTILS_H
#define INC_TRACER_UTILS_H

#include <map>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "pin.H"
#include "instlib.H"
#include "trace_serializer.pb.h"

typedef uint64_t address_t;
enum endianness {
	LITTLE, BIG
};

struct addressRange {
	ADDRINT lowerBound;
	ADDRINT upperBound;

	size_t getSize () {
		return (size_t) (upperBound - lowerBound + 1);
	}
};

struct segment {
public:
	string name;
	string fileName;
	int permissions;
	endianness endian;
	ADDRINT address;
	ADDRINT size;
	const char* data;

	static int encodePermissions (BOOL readable, BOOL writable, BOOL executable);
	void serialize (ofstream& file);
private:
	void serializeProtoBuffers (ofstream& file);
	void serializeRaw (ofstream& file);
};

struct module {
public:
	void add (segment* seg);
	void serialize (ofstream& file);
private:
	vector<segment*> segments;
	void serializeProtoBuffers (ofstream& file);
	void serializeRaw (ofstream& file);
};

struct registerValues {
public:
	ADDRINT programAddress;
	map<string, ADDRINT> regValues;

	void serialize (ofstream& file);
private:
	void serializeProtoBuffers (ofstream& file);
	void serializeRaw (ofstream& file);
};

struct controlFlowTrace {
public:
	void add (ADDRINT from, ADDRINT to, bool jumpTaken);
	void serialize (ofstream& file);
private:
	struct controlFlowTuple {
		ADDRINT from;
		ADDRINT to;
		bool jumpTaken;
	};
	vector<controlFlowTuple*> controlFlows;
	void serializeProtoBuffers (ofstream& file);
	void serializeRaw (ofstream& file);
};

struct mallocTrace {
public:
	void addMallocCall (ADDRINT callSite, ADDRINT returnSite, ADDRINT sizeArgument, ADDRINT returnValue);
	void addFreeCall (ADDRINT callSite, ADDRINT returnSite, ADDRINT pointerArgument);
	void serialize (ofstream& file);
private:
	traces::MallocTrace trace;
	void serializeProtoBuffers (ofstream& file);
};

const string toHex (address_t address);

address_t parseHex (string number);

char* dumpMemoryRange (addressRange range);

/**
 * Print the disassembled instructions of a function.
 */
string disassembleFunction (RTN function);

/**
 * Print all the sections in the image.
 */
string dumpSections (IMG image);

/**
 * Print all the symbols in the image. Dynamic symbols are prepended with a "d".
 */
string dumpSymbols (IMG image);

/**
 * Print all the files that were loaded (e.g. dynamically linked in files) during the execution.
 */
string dumpLoadedFiles ();


/**
 * Returns the start address of the main executable.
 */
ADDRINT getProgramEntryPoint();

/**
 * Returns the endianness of the current architecture.
 */
endianness getEndianness ();

/**
 * Return the memory map of the current process on Linux.
 */
string getMemMapFromLinuxProc ();

/**
 * Dumps a named segment by parsing the linux memmap of the process and getting the properties of the segment from there.
 * Returns NULL if the segment was not found.
 */
segment* dumpNamedSegmentFromMemMap (string name);

ADDRINT getProgramCounterValue (const CONTEXT* ctx);

ADDRINT getValueOnTopOfStack (const CONTEXT* ctx);

#endif
