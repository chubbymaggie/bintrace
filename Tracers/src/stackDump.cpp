#include "tracerUtils.h"
#include "Dumper.h"

using namespace std;

class StackDumper: public virtual Dumper {
	string dumpFileName;
	addressRange stackRange;

public:
	void init (string outputFileNamePrefix) {
		dumpFileName = outputFileNamePrefix + ".stack";
		// add instrumentation at program start
		PIN_AddThreadStartFunction(onThreadStart, this);
	}

	void checkPointDump (string dumpSuffix, THREADID threadID, const CONTEXT* ctx) {
		stackRange.lowerBound = PIN_GetContextReg(ctx, REG_STACK_PTR);
#ifdef DEBUG
		LOG("[checkpoint] sp=" + toHex(stackRange.lowerBound) + "\n");
#endif
		string fileName(dumpFileName + "." + dumpSuffix);
		dumpToFile(fileName);
	}

	void startTracing (THREADID threadID, const CONTEXT* ctx) {
		// the beginning address of the stack is set at thread start
		stackRange.lowerBound = PIN_GetContextReg(ctx, REG_STACK_PTR);
#ifdef DEBUG
		LOG("[start] sp=" + toHex(stackRange.lowerBound) + "\n");
#endif
		getRangeFromMemMap(stackRange);
		dumpToFile(dumpFileName);
	}

	void stopTracing (THREADID threadID, const CONTEXT* ctx) {
#ifdef DEBUG
		ADDRINT sp = PIN_GetContextReg(ctx, REG_STACK_PTR);
		LOG("[stop] sp=" + toHex(sp) + "\n");
		LOG(getMemMapFromLinuxProc() + "\n");
#endif
	}

private:
	static void onThreadStart (THREADID threadID, CONTEXT* ctx, INT32 exitCode, void* payload) {
		StackDumper* dumper = static_cast<StackDumper*>(payload);
		dumper->stackRange.upperBound = PIN_GetContextReg(ctx, REG_STACK_PTR);
#ifdef DEBUG
		LOG("[threadstart] sp=" + toHex(dumper->stackRange.upperBound) + "\n");
#endif
	}

	void dumpToFile (string fileName) {
		ofstream dumpFile;
		dumpFile.open(fileName.c_str());
		segment stackSegment;
		stackSegment.name = "stack";
		stackSegment.fileName = "(none)";
		stackSegment.address = stackRange.lowerBound;
		stackSegment.endian = getEndianness();
		stackSegment.size = stackRange.getSize();
		stackSegment.permissions = getPermissionsFromMemMap();
		if (stackSegment.permissions == -1)
			stackSegment.permissions = stackSegment.encodePermissions(TRUE, TRUE, FALSE); // some sane defaults
		stackSegment.data = dumpMemoryRange(stackRange);
		stackSegment.serialize(dumpFile);
		dumpFile.close();
	}

	/**
	 * Parse the memory map to find the access rights of the stack. The expected format is:
	 * 7ffff3b1e000-7ffff3b3f000 rw-p 00000000 00:00 0  [stack]
	 * Returns the permissions binary encoded similar to the unix file access rights.
	 */
	int getPermissionsFromMemMap () {
		string memMap = getMemMapFromLinuxProc();
		BOOL readable = false;
		BOOL writable = false;
		BOOL executable = false;
		istringstream stream(memMap);
		string line;
		ADDRINT address;
		while (getline(stream, line)) {
			if (line.find("[stack]") == string::npos)
				continue;
			stringstream lineStream(line);
			lineStream >> hex >> address;
			lineStream.ignore(1); // "-" separator
			lineStream >> hex >> address;
			lineStream.ignore(1); // the space character separator
			char flag;
			lineStream >> flag;
			if (flag != '-')
				readable = true;
			lineStream >> flag;
			if (flag != '-')
				writable = true;
			lineStream >> flag;
			if (flag != '-')
				executable = true;
			return segment::encodePermissions(readable, writable, executable);
		}
		return -1;
	}

	/**
	 * Parse the memory map to find the address range of the stack. The expected format is:
	 * 7ffff3b1e000-7ffff3b3f000 rw-p 00000000 00:00 0  [stack]
	 */
	bool getRangeFromMemMap (addressRange& memoryRange) {
		string memMap = getMemMapFromLinuxProc();
		istringstream stream(memMap);
		string line;
		while (getline(stream, line)) {
			if (line.find("[stack]") == string::npos)
				continue;
			stringstream lineStream(line);
			lineStream >> hex >> memoryRange.lowerBound;
			lineStream.ignore(1); // "-" separator
			lineStream >> hex >> memoryRange.upperBound;
			memoryRange.upperBound = memoryRange.upperBound - 1;
			return true;
		}
		return false;
	}

};

