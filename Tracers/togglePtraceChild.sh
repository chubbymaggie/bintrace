#!/bin/sh
set -e
ptraceSwitchLocation="/proc/sys/kernel/yama/ptrace_scope"
switch=$(cat "$ptraceSwitchLocation")
case "$switch" in
 0) switch=1; echo "turning ptrace child only on.";;
 1|*) switch=0; echo "turning ptrace child only off.";;
esac

echo "$switch" > "$ptraceSwitchLocation"
